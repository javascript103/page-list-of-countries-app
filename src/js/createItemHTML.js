const regExp = /(?=\B(?:\d{3})+(?!\d))/g;

// eslint-disable-next-line import/prefer-default-export
export function createItemHTML(item) {
  const descName = typeof item.name === 'string' ? item.name : item.name.common;
  let population = item.population / 1000000;
  population = population.toString().replace(regExp, ' ').replace('.', ',');

  const itemElement = document.createElement('div');
  itemElement.classList.add('countries-list__item');
  itemElement.innerHTML = `
    <div class="countries-list__item-flag">
      <img src="${item.flags.svg}" class="countries-list__item-flag__img">
    </div>
    <div class="countries-list__item-desc">
      <p class="countries-list__item-desc__name">${descName}</p>
      <p class="countries-list__item-desc__population"><b>Population: </b>${population}</p>
      <p class="countries-list__item-desc__region"><b>Region: </b>${item.region}</p>
      <p class="countries-list__item-desc__capital"><b>Capital: </b>${item.capital}</p>
    </div>
  `;
  return itemElement;
}
