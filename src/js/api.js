import axios from 'axios';

export async function fetchAllItems() {
  const url = 'https://restcountries.com/v2/all';
  const response = await axios.get(url);
  return response.data;
}

export async function fetchCountryByName(name) {
  const urlName = `https://restcountries.com/v2/name/${name}`;
  const response = await axios.get(urlName);
  return response.data;
}

export async function fetchCountryByRegion(region) {
  const urlRegion = `https://restcountries.com/v3.1/region/${region}`;
  const response = await axios.get(urlRegion);
  return response.data;
}
