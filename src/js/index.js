import '../styles/reset.css';
import '../styles/style.scss';

import { fetchAllItems, fetchCountryByName, fetchCountryByRegion } from './api';
import { renderItemList, clearItemsList } from './renderItemList';

const searchButtonNameElement = document.querySelector('#country-search__name-button');
const searchInputElement = document.querySelector('#country-search__name-input');
const searchButtonRegionElement = document.querySelector('.country-search__region-button');
const searchButtonRegionDropdownElement = document.querySelectorAll('.country-search__region-dropdown__button');
const changeThemeButton = document.querySelector('.main-header__theme-button');
const changeThemeButtonImg = document.querySelector('.main-header__theme-button__img');
const changeThemeButtonDesc = document.querySelector('.main-header__theme-button__desc');
const themeElement = document.querySelector('#theme-color');
const resetFilterElement = document.querySelector('.country-search__reset');
const resetFilterButtonElement = document.querySelector('.country-search__reset-button');

async function searchByName(countryName) {
  const item = await fetchCountryByName(countryName);
  clearItemsList();
  renderItemList(item);
  searchInputElement.value = '';
  resetFilterElement.classList.add('active');
  searchButtonRegionElement.textContent = 'Filter by Region';
}

async function searchByRegion(event) {
  const regionElement = event.target.dataset.region;
  const item = await fetchCountryByRegion(regionElement);
  clearItemsList();
  renderItemList(item);
  resetFilterElement.classList.add('active');
  searchButtonRegionElement.textContent = regionElement;
}

function changeTheme() {
  if (themeElement.classList.contains('light-theme')) {
    themeElement.classList.remove('light-theme');
    themeElement.classList.add('dark-theme');
    changeThemeButtonDesc.textContent = 'Light mode';
    changeThemeButtonImg.src = 'assets/icons/sun.svg';
    changeThemeButtonImg.classList.add('main-header__theme-button__sun');
    changeThemeButtonImg.classList.remove('main-header__theme-button__img');
    resetFilterButtonElement.classList.add('change-color');
  } else {
    themeElement.classList.remove('dark-theme');
    themeElement.classList.add('light-theme');
    changeThemeButtonDesc.textContent = 'Dark mode';
    changeThemeButtonImg.src = 'assets/icons/moon.svg';
    changeThemeButtonImg.classList.add('main-header__theme-button__img');
    changeThemeButtonImg.classList.remove('main-header__theme-button__sun');
    resetFilterButtonElement.classList.remove('change-color');
  }
}

async function resetFilter() {
  clearItemsList();
  const items = await fetchAllItems();
  renderItemList(items);
  resetFilterElement.classList.remove('active');
  searchButtonRegionElement.textContent = 'Filter by Region';
}

async function initApp() {
  const items = await fetchAllItems();
  renderItemList(items);
  searchButtonRegionDropdownElement.forEach((item) => {
    item.addEventListener('click', searchByRegion);
  });
  searchButtonNameElement.addEventListener('click', () => searchByName(searchInputElement.value));
  changeThemeButton.addEventListener('click', changeTheme);
  resetFilterElement.addEventListener('click', resetFilter);
}

initApp();
