import { createItemHTML } from './createItemHTML';

const itemListElement = document.querySelector('.countries-list');

// eslint-disable-next-line import/prefer-default-export
export function renderItemList(items) {
  items.forEach((item) => {
    const itemElement = createItemHTML(item);
    itemListElement.append(itemElement);
  });
}

export function clearItemsList() {
  itemListElement.innerHTML = '';
}
